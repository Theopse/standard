defmodule Standard.Access do
  import Standard

  begin "Macro's Define" do
    defmacro get(container, key, default \\ nil)
  end

  begin "Macro's" do
    defmacro get(container, key, default) do
      quote do
        case Access.get(unquote(container), unquote(key)) do
          nil -> unquote(default)
          value -> value
        end
      end
    end
  end
end
