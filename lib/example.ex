defmodule Standard.Example do
  @spec f1(non_neg_integer()) :: non_neg_integer()
  def f1(n) when is_integer(n) and n > 0, do: f1(n, 0)

  defp f1(0, r), do: r
  defp f1(n, r) when n > 100_0000, do: f1(100_0000, r + (n - 100_0000) * 0.01)
  defp f1(n, r) when n > 60_0000, do: f1(60_0000, r + (n - 60_0000) * 0.015)
  defp f1(n, r) when n > 40_0000, do: f1(40_0000, r + (n - 40_0000) * 0.03)
  defp f1(n, r) when n > 20_0000, do: f1(20_0000, r + (n - 20_0000) * 0.05)
  defp f1(n, r) when n > 10_0000, do: f1(10_0000, r + (n - 10_0000) * 0.075)
  defp f1(n, r), do: f1(0, r + n * 0.1)
end
