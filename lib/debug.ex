defmodule Standard.Debug do
  @spec code(list()) :: binary
  def code(quoted) do
    quoted
    |> Macro.expand(__ENV__)
    |> Macro.to_string()
  end

  @spec mcode(any) :: binary
  defmacro mcode(quoted) do
    quoted
    |> Macro.expand(__ENV__)
    |> Macro.to_string()
  end
end
