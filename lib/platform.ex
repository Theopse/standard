defmodule Standard.Platform do

  def name do
    case :os.type do
      {:win32, _} ->
        # File.exists?("C:\\Windows\\SysWOW64") && {:windows, 64} || {:windows, 32}
        :windows
      {:unix, platform} ->
        case platform do
          :darwin ->
            :macos
          default -> default
        end
    end
  end

end
