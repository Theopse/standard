defmodule StandardTest do
  use ExUnit.Case
  doctest Standard
  import Standard

  test "greets the world" do
    assert Standard.hello() == :world
    pipe([&+/2, {2,Enum.reduce([1,2,3])}])
  end

  test "nyi" do
    try do
      nyi({:test})
    catch
      :exit, :nyi -> :ok
    end
  end
end
